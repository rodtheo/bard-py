# BARD Algorithm Implementation

It contains my python implementation of the algorithm described by Bardwell et al [1] to detect abnormal segments in multiple time series. You can have a look at [ipython notebook](http://nbviewer.jupyter.org/urls/bitbucket.org/rodtheo/bard-py/raw/6f831e06f4da0f26cea06da2b67e380c728e3833/bard-implemented-python.ipynb) showing and example of usage.

[1] Bardwell, Lawrence, and Paul Fearnhead. 2016. �Bayesian Detection of Abnormal Segments in Multiple Time Series.� Bayesian Analysis, February. doi:10.1214/16-BA998.