from scipy.misc import logsumexp
from scipy.special import gammaln
from scipy.misc import factorial
from scipy.misc import comb
import numpy as np
from scipy.stats import nbinom
import json
import os

# Calculating log P.A(s,t) for segment (s,t) integrated on uniform prior over mu_seq
# done without normal part at front
# p = 0.04

# EN = (k_N * (1-p_N)) / p_N
# EA = (k_A * (1-p_A)) / p_A

# # Define the initial distribution for X_1 = (B1,C1), first point. Note that C_1 = 0,
# # the previous cp is in location zero, so we need only a distribution for B_1.
# # Thus see paper before section 2.2

# ldenom = np.log(pi_N*EN + EA)
# qA = np.log(pi_N) + np.log(EN) - ldenom
# qN = np.log(EA) - ldenom
## P_A precisa de N, p e S, mu_seq
def P_N(s,t,dt):
    lambd=10
    N = dt.shape[1]
    sumSandD = np.sum(np.sum(dt[s:t+1,], axis=1))
    return (sumSandD)*np.log(lambd)-N*(s-t+1)*lambd

def P_A(s,t,mu_seq,S,N,p):
    lambd=10
    rgamma=6.25
    sgamma=2.5
    dt=S
    # width of rectangle
    mu_wid = np.diff(mu_seq)[0]
    # vector to populate
    vec = np.zeros(len(mu_seq))
    # evaluate at each point of grid
    # do this as typically smaller than dimension
    # evaluating log of quantity
    sumS = np.sum(dt[s:t+1,], axis=1)
    for k in xrange(0,len(mu_seq)):
        vec[k] = np.sum(p*np.exp(-mu_seq[k]*(s+t-1))*mu_seq[k]**(sumS) + (1-p)*np.exp(-lambd*(s-t+1))*lambd**(sumS)) + rgamma*np.log(sgamma) - gammaln(rgamma) + (rgamma-1)*np.log(mu_seq[k]) - s*mu_seq[k]
    cmax = np.amax(vec)
    marg_like = cmax + logsumexp(vec - cmax) + np.log(mu_wid)
    return marg_like

# def P_N(s,t,dt):
#     N = dt.shape[1]
#     vec = -0.5*np.sum(dt[s:t+1,]**2, axis=0)
#     logs = np.sum(vec)
#     return logs

# def P_A(s,t,mu_seq,S,N,p):
#     # prior value mu_dens
#     mu_dens = 1./(mu_seq[-1] - mu_seq[0])
#     # width of rectangle
#     mu_wid = np.diff(mu_seq)[0]
#     vec = np.zeros(len(mu_seq))
#     # evaluate at each point of grid
#     # do this as typically smaller than dimension
#     # evaluating log of quantity
#     for k in xrange(0,len(mu_seq)):
#         vec[k] = N*np.log(1-p) + np.sum(np.log(1 + np.exp(mu_seq[k] * (S[t + 1,:] - S[s,:] - mu_seq[k] * ((t-s+1)/2.)) + np.log(p) - np.log(1-p))))
#     #finding sum of logs -- for numerical instability
#     cmax = np.amax(vec)
#     marg_like = cmax + logsumexp(vec - cmax) + np.log(mu_dens) + np.log(mu_wid)

#     return marg_like

# Resampling function -- takes a vector of weights that are < alpha 
# to resample and does SRC resampling, then returns a vector with resampled 
# weights 

def resample(to_resample, alpha):
    log_alpha = np.log(alpha)
    log_u = log_alpha + np.log(np.random.uniform(0,1,1))
    k = 0
    while k <= len(to_resample)-1:
        # want to find u <- u-w then if u <=0
        # u<-u+alpha
        # IN OUR CASE IF u <= w
        # find u <- u + alpha - w
        # as working with logs
        # SO first check whether log(u) < log(w)
        if log_u < to_resample[k]:
            # u <- u + alpha - w
            # first find log(alpha - w) label as log.alpha.weight
            temp = np.append(log_alpha, to_resample[k])
            c = np.amax(temp)
            log_alpha_weight = c + np.log(np.exp(temp[0] - c) - np.exp(temp[1] - c))
            temp = np.append(log_u, log_alpha_weight)
            c = np.amax(temp)
            log_u = c + np.log(np.sum(np.exp(temp - c)))
            to_resample[k] = log_alpha
        else:
            # u <- u - w
            # and w is not resampled, given weight 0 (-Inf for log(w))
            temp = np.append(log_u, to_resample[k])
            c = np.amax(temp)
            log_u = c + np.log(np.exp(temp[0] - c) - np.exp(temp[1]-c))
            to_resample[k] = -np.inf
        k = k + 1
    return(to_resample)

def pnbinomf(k, n, p, lower_tail=True):
    if lower_tail:
        fin = nbinom.logcdf(k, n, p)
    else:
        fin = nbinom.logsf(k, n, p)
    return fin

def dnbinom(x, n, p, lower_tail=True):
    if lower_tail:
        logdnbinom = nbinom.logpmf(x, n, p)
    else:
        logdnbinom = np.log(1 - nbinom.pmf(x,n,p))
    return logdnbinom

def SRC_filtering(data, affected_dim, model_name, model_file, alpha=1e-4):

    with open(model_file, "r") as data_file:
        info = json.load(data_file)

    mu_seq = np.arange(5., 20., 2.5)

    #length and dimension of data
    n = data.shape[0]
    N = data.shape[1]

    # data summaries etc
    #S = np.concatenate([np.zeros((1,data.shape[1])), np.cumsum(data, axis=0)] )
    S = data
    S_2 = np.cumsum(np.append(np.array([0]), np.sum(data**2, axis=1)))

    # p is used to calc the marginal like for abnormal
    # ratio of abnormal profiles
    p = float(affected_dim) / N

    ## ROD COMMENT

    k_A = info['model'][model_name]['k_A']
    p_A = info['model'][model_name]['p_A']

    k_N = info['model'][model_name]['k_N']
    p_N = info['model'][model_name]['p_N']

    pi_A = info['model'][model_name]['pi_A']
    pi_N = info['model'][model_name]['pi_N']
    # simulate from the model -- data like from the first simulation study 
    # in the paper, dimension 200 and length approx 1000

    # fixed no of dimensions and no of affected dimensions i.e. 4% 

    EN = (k_N * (1-p_N)) / p_N
    EA = (k_A * (1-p_A)) / p_A

    # Define the initial distribution for X_1 = (B1,C1), first point. Note that C_1 = 0,
    # the previous cp is in location zero, so we need only a distribution for B_1.
    # Thus see paper before section 2.2

    ldenom = np.log(pi_N*EN + EA)
    qA = np.log(pi_N) + np.log(EN) - ldenom
    qN = np.log(EA) - ldenom


    # useful to define log of resampling probability
    log_alpha = np.log(alpha)
    # lists for filtering, probs, location and types of segments
    weights = np.ones((n,2*n)) * -np.inf
    locations = np.ones((n,2*n)) * -np.inf
    typev = np.ones((n,2*n)) * -np.inf
    # type - 0 for normal segment 1 for abnormal segment

    # initial N
    curr_locations = np.empty(2)
    curr_weights = np.empty(2)
    curr_type = np.empty(2)

    curr_locations[0] = -1
    curr_weights[0] = qN + P_N(0,0,data)
    curr_type[0] = 0

    # initial A
    curr_locations[1] = -1
    curr_weights[1] = qA + P_N(0,0,data) + P_A(0, 0 , mu_seq, S, N, p)
    curr_type[1] = 1

    c = np.amax(curr_weights)
    weights[0,0:2] = curr_weights - ( c + logsumexp(curr_weights - c))
    locations[0,0:2] = curr_locations
    typev[0,0:2] = curr_type
    #print "first w", curr_weights, weights[0,:]

    for t in xrange(1,n):
        prev_type = typev[t-1,0:t*2]
        prev_locs = locations[t-1,0:t*2]
        prev_weights = weights[t-1,0:t*2]

        #print "second w", prev_weights

        curr_type = typev[t,0:t*2]
        curr_locs = locations[t,0:t*2]
        curr_weights = weights[t,0:t*2]

        #print "next w", curr_weights

        # label support points make it easiar to fnd positions in matrices
        sup_point_N = np.where(prev_type == 0)[0]
        sup_point_A = np.where(prev_type == 1)[0]

        #print("N sup", sup_point_N)
        #print("A sup", sup_point_A)
        #print("prev w", prev_weights)
        #print("prev l", prev_locs)

        # propagate normal particles
        # carry on in N state, nbinom.len
        # and P_N marginal likelihood
        if len(sup_point_N) > 0:
            for j in np.nditer(sup_point_N):
                i = prev_locs[j]
                nbinom_len = pnbinomf(t-i-1, k_N,p_N,lower_tail=False) - pnbinomf(t-i-2,k_N,p_N,lower_tail=False)
                curr_weights[j] = prev_weights[j] + nbinom_len + P_N(i+1,t,data) - P_N(i+1,t-1,data)

        # i = prev_locs[sup_point_N]
        # nbinom_len = pnbinomf(t-i-1, k_N, p_N, lower_tail=False) - pnbinomf(t-i-2,k_N,p_N,lower_tail=False)
        # curr_weights[sup_point_N] = prev_weights[sup_point_N] + nbinom_len - 0.5*(S_2[t+1]-S_2[t])
        #print curr_weights

        # propagate abnormal particles
        # carry on in A state, nbinom.len
        # Marginal likelihood PA calculated at i+1:t and i+1:t-1
        #print "sup_point_A", sup_point_A
        #print "sup_point_N", sup_point_N

        if len(sup_point_A) > 0:
            for j in np.nditer(sup_point_A):
                #print j
                i = prev_locs[j]
                nbinom_len = pnbinomf(t-i-1,k_A,p_A,lower_tail=False) - pnbinomf(t-i-2,k_A,p_A,lower_tail=False)
                #print nbinom_len
                curr_weights[j] = prev_weights[j] + nbinom_len + P_N(i+1,t,data) + P_A(i+1,t,mu_seq,S,N,p) - P_A(i+1, t-1, mu_seq,S,N,p) - P_N(i+1,t-1,data)
        #print "SECOND", curr_weights
        ################################################
        # Calc other point chpt - C_t = t-1 , B_t = N/A
        ################################################

        ####
        # B_t = N
        ####
        # transition from abnormal to normal/ if there are no abnormal pts
        # give it a weight of zero i.e. log weight = - Inf
        i = prev_locs[sup_point_A]

        if (len(i) == 0):
            C_N = - np.inf

        else:
            # weight to propagate
            W = prev_weights[sup_point_A] + dnbinom(t-i-1,k_N,p_N) - pnbinomf(t-i-2,k_N,p_N,False) + np.log(pi_N)
            # transition from A - N
            # (only trans possible to get to N)
            # prob of pi_N
            #print("prev A", prev_weights[sup_point_A])
            #print("W", W)
            #print("t",t)
            cmax = np.amax(W)
            #print("cmax", cmax)
            C_N = P_N(t,t,data) + cmax + logsumexp(W - cmax)

        #print("C_N",C_N)
        ####
        # B_t = A
        ####
        # transition to abnormal, can be A-A or N-A
        i_N = prev_locs[sup_point_N]
        i_A = prev_locs[sup_point_A]
        i_N = np.array(i_N)
        #print "i_N", i_N
        if len(i_N) == 0:
            W = prev_weights[sup_point_A] + dnbinom(t - i_A - 1, k_A,p_A) - pnbinomf(t - i_A - 2,k_A,p_A,False) 
            cmax = np.amax(W)
            C_A = P_A(t,t,mu_seq,S,N,p) + P_N(t,t,data) + np.log(pi_A) + cmax + logsumexp(W-cmax)

        elif len(i_A) == 0:

            # no abnormal particles just use N particles
            W = prev_weights[sup_point_N] + dnbinom(t-i_N-1,k_N,p_N) - pnbinomf(t-i_N-2,k_N,p_N,False)
            cmax = np.amax(W)
            C_A = P_A(t,t,mu_seq,S,N,p) + P_N(t,t,data) + cmax + logsumexp(W - cmax)
        else:
            #each have some N and A particles do seperately
            W = prev_weights[sup_point_A] + dnbinom(t-i_A-1,k_A,p_A) - pnbinomf(t-i_A-2,k_A,p_A,False)
            cmax = np.amax(W)
            ABS_PART = np.log(pi_A) + cmax + logsumexp(W - cmax)
            W = prev_weights[sup_point_N] + dnbinom(t-i_N-1,k_N,p_N) - pnbinomf(t-i_N-2,k_N,p_N,False)
            cmax = np.amax(W)
            NORM_PART = cmax + logsumexp(W - cmax)
            # put these together
            part = np.append(NORM_PART, ABS_PART)
            cmax = np.amax(part)
            C_A = P_A(t,t,mu_seq,S,N,p) + P_N(t,t,data) + cmax + logsumexp(part - cmax)
        ###############################################
        ## resample if neccessary only when t > 20 ##
        ###############################################

        if (t > 20):
            temp_weights = np.append(curr_weights, [C_N, C_A])
            temp_locs = np.append(prev_locs, [t, t])
            temp_type = np.append(prev_type, [0, 1])

            ## log normalized weights
            c = np.amax(temp_weights)
            lognorm_weights = temp_weights - (c + logsumexp(temp_weights-c))

            ######################
            ## stratified resampling part
            ######################

            # take all the log weights that are < alpha and resample them
            to_resample =  lognorm_weights[lognorm_weights < log_alpha]
            # resampling function in resampe.r
            lognorm_weights[lognorm_weights < log_alpha] = resample(to_resample, alpha)

            # remove weights for which = -Inf
            exclude = np.where(lognorm_weights == -np.inf)
            temp_locs[exclude] = -np.inf
            updated_locs = temp_locs
            temp_type[exclude] = -np.inf
            updated_type = temp_type
            #toinclude = np.where(lognorm_weights != -np.inf)
            #update_locs = temp_locs[toinclude]
            #update_type = temp_type[toinclude]

            #renormalise weights - is this necessary?
            temp_weights = lognorm_weights
            c = np.amax(temp_weights)
            renorm_weights = temp_weights - (c + np.log(np.sum(np.exp(temp_weights-c))))

            # put it back in vectors (ordered)
            weights[t,0:(t+1)*2] = renorm_weights
            locations[t,0:(t+1)*2] = updated_locs
            typev[t,0:(t+1)*2] = updated_type
            #print "type", typev

        ######
        ## no resampling t <= 20
        ######
        else:
            temp_weights = np.append(curr_weights,[C_N, C_A])
            #print "temp w", temp_weights
            # find log normalised weights
            c = np.amax(temp_weights)
            lognorm_weights = temp_weights - (c + np.log(np.sum(np.exp(temp_weights-c))))
            #print "lognorm",lognorm_weights, lognorm_weights.shape
            limit = len(lognorm_weights)
            weights[t,0:(t+1)*2] = lognorm_weights
            locations[t,0:(t+1)*2] = np.append(prev_locs, [t, t])
            typev[t,0:(t+1)*2] = np.append(prev_type, [0, 1])
            #print "type", typev
            #print "locations", locations[t,:]
    return (weights, locations, typev)

def draw_from_post(logprobs, cpt_locs, types, info, model_name):
    
    n = logprobs.shape[0]-1
    l_probs = logprobs[n,:]
    seg_locs = cpt_locs[n,:]
    seg_types = types[n,:]
    

    exclude_inf = np.invert(np.isinf(l_probs))
    l_probs = l_probs[exclude_inf]
    seg_locs = seg_locs[exclude_inf]
    seg_types = seg_types[exclude_inf]

    #print np.exp(l_probs), seg_locs, seg_types
    
    ind = np.arange(0,len(l_probs))
    a = np.random.choice(ind, 1, replace=True, p=np.exp(l_probs))
    
    # state and location
    curr_state = seg_types[a]
    t = int(seg_locs[a][0])
    #print "t",t
    STATES = [curr_state]
    DRAW = [t]

    ## ROD COMMENT

    pi_A = info['model'][model_name]['pi_A']
    pi_N = info['model'][model_name]['pi_N']

    k_A = info['model'][model_name]['k_A']
    p_A = info['model'][model_name]['p_A']

    k_N = info['model'][model_name]['k_N']
    p_N = info['model'][model_name]['p_N']

    ## END ROD
    while t > 0:
        if curr_state == 0:
            #print "types 1",(types[t,:] == 1)
            l_probs = logprobs[t,:][types[t,:] == 1]
            i = cpt_locs[t,:][types[t,:] == 1]
            back_dens = np.log(pi_N) + dnbinom(t-i-1,k_A,p_A) - pnbinomf(t-i-2,k_A,p_A, lower_tail=False)
            ##l_probs = np.logaddexp(l_probs,back_dens)
            l_probs = l_probs + back_dens
            
            ind = np.arange(len(l_probs))
            #probs = l_probs
            #probs /= probs.sum().astype(float)
            
            l_probs = l_probs - logsumexp(l_probs)
            #print np.sum(np.exp(l_probs))
            #print np.exp(l_probs)
            a = np.random.choice(ind, 1, replace=True, p=np.exp(l_probs))
            
            #state and location
            curr_state = 1
            t = int(i[a][0])
            #print "t rod", t
            STATES.append(curr_state)
            DRAW.append(t)
            
        else:
            #print "abnormal state"
            #currently in abnormal state poss transitions to normal or abnormal
            i_N = cpt_locs[t,:][types[t,:] == 0]
            i_A = cpt_locs[t,:][types[t,:] == 1]
            #print "inf",cpt_locs[t,:],[types[t,:] == 0],i_N
            

            l_probsA = logprobs[t,:][types[t,:] == 1]

            back_densA = np.log(pi_A) + dnbinom(t-i_A-1, k_A, p_A) - pnbinomf(t-i_A-2,k_A,p_A,lower_tail=False)
            ##l_probsA = np.logaddexp(l_probsA,back_densA)
            l_probsA = l_probsA + back_densA
            #normal points
            l_probsN = logprobs[t,:][types[t,:] == 0]
            back_densN = dnbinom(t-i_N-1,k_N,p_N) - pnbinomf(t-i_N-2,k_N,p_N, lower_tail=False)
            ##l_probsN = np.logaddexp(l_probsN,back_densN)
            l_probsN = l_probsN + back_densN
            #print "l_s", np.exp(l_probsA), l_probsN
            # now normalise
            i_all = np.append(i_N,i_A)
            t_all = np.append(np.repeat(0, len(i_N)), np.repeat(1,len(i_A)))
            l_probs = np.append(l_probsN, l_probsA)

            c = np.amax(l_probs)
            c_w = np.argmax(l_probs)
            #l_probs_norm = l_probs - (c + np.log(np.sum(np.exp(l_probs-c))))
            ##l_probs_norm = l_probs - (c + logsumexp(l_probs -c))
            #l_probs_norm = l_probs - (logsumexp(l_probs))
            l_probs_norm = l_probs - (c + logsumexp(l_probs - c))
            #print "c",l_probs_norm,c
            ind = np.arange(0,len(l_probs_norm))
            #print len(l_probs_norm), np.sum(np.exp(l_probs_norm)), c, i_all[c_w]
            #print "log probs norm", np.exp(l_probs_norm), np.exp(l_probs_norm).sum()
            a = np.random.choice(ind, 1, replace=True, p=np.exp(l_probs_norm))
            
            # state and location
            curr_state = int(t_all[a])
            t = int(i_all[a][0])
            #print curr_state, t
            STATES.append(curr_state)
            DRAW.append(t)
    
    DRAW = np.array(DRAW, dtype=int)
    DRAW = np.append(np.array([n-1]), DRAW)
    DRAW[DRAW == -1] = 0
    STATES = np.array(STATES, dtype=int)
    
    return(DRAW, STATES)

def get_prob_vec(ww, ll, tt, model_name, model_file, no_draws = 1000):
    n = ww.shape[0]
    log_probs = ww
    cpt_locs = ll
    types = tt
    

    # vec is a vector of counts of seg locations
    vec = np.zeros(n)
    prob_states = np.ones((no_draws,n)) * -np.inf
    
    with open(model_file, "r") as data_file:
        info = json.load(data_file)

    for i in xrange(0,no_draws):
        draws,stat = draw_from_post(log_probs, cpt_locs, types, info, model_name)
        segs = draws
        states = stat
        
        #print("draws", segs)
        #print("states", states)
        
        #print "draws",draws,stat
        # fill vec (hist of chpts)
        vec[draws] = vec[draws] + 1
        
        # probs of being N or A
        prob_states[i,] = get_states(segs, states, n)
        #print "prob_states", prob_states[i,]
        
    return np.sum(prob_states, axis=0)/no_draws

def get_states(segs, states, n):
    
    state_vec = np.zeros(n)
    state_vec[segs[1]:segs[0]] = states[0]
    
    for i in xrange(1, len(segs)-1):
        state_vec[segs[i+1]:segs[i]-1] = states[i]
        
    return state_vec

# apply loss function to get a segmentation returns vector with 0's and 1's
# 0 means in a normal segment , 1 is abnormal
# apply loss with parameter gamma

def loss(gamma, probvec):
    p_gamma = 1./(1+gamma)
    probvec[probvec < p.gamma ] = 0
    probvec[probvec>0] = 1
    
    return(probvec)